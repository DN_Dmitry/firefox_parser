#include "parsff.h"
#include <qmenubar.h>
#include <qfiledialog.h>
#include <qxmlstream.h>
#include <qinputdialog.h>
#include <qdebug.h>
#include <QMessageBox>
#include <qtextcodec.h>
#include <QListView>
#include <QDesktopServices>
#include <tools.hxx>
#include "resource.h"


ParsFF::ParsFF(QWidget *parent)
	: QWidget(parent)
{		
	QFile stylesFile("stylesheet.qss");
	stylesFile.open(QFile::ReadOnly);
	QString StyleSheet = QLatin1String(stylesFile.readAll());
	this->setStyleSheet(StyleSheet);	
	this->setWindowIcon(QIcon("icon/favicon.ico"));
	ui.setupUi(this);	
	ui.lbPic->setVisible(false);	
	ui.tvView->alternatingRowColors();
	ui.tvView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tvView->setSelectionMode(QAbstractItemView::SingleSelection);
	ui.tvView->horizontalHeader()->setStretchLastSection(true);
	this->initTreeView();
	ui.pbCancel->hide();
	ui.cbWhat->setView(new QListView());
	ui.cbCount->setView(new QListView());

	QMenuBar* menuBar = new QMenuBar(this);
	m_fileMenu = new QMenu(tools::loadStringFromResource(IDS_MENU_FILE), this);
	menuBar->addMenu(m_fileMenu);
	m_fileMenu->addAction(tools::loadStringFromResource(IDS_ACT_OPEN), this, SLOT(fileDialog()));
	m_fileMenu->addAction(tools::loadStringFromResource(IDS_ACT_OPEN_CACHE_FOLDER), this, SLOT(dirDialog()));
	m_fileMenu->addSeparator();
	m_fileMenu->addAction(tools::loadStringFromResource(IDS_ACT_EXPORT_ALL), this, SLOT(exporting()));
	m_fileMenu->addAction(tools::loadStringFromResource(IDS_ACT_SEARCH), this, SLOT(search()));	
	m_fileMenu->addSeparator();
	m_fileMenu->addAction(tools::loadStringFromResource(IDS_ACT_CLOSE), this, SLOT(close()));
	ui.verticalLayout->addWidget(menuBar);	
	
	ui.tvView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_model = new QStandardItemModel(this);
	checkFileMenu();

	m_player = new QMediaPlayer(this);
	ui.pbPlay->setEnabled(false);
	ui.pbPause->setEnabled(false);
	ui.pbStop->setEnabled(false);	

	initCombo();
	ui.pbUpdate->setEnabled(false);
	ui.pbNext->setEnabled(false);		
	m_nBookmark = 0;
	ui.pbPrev->setEnabled(false);	
	ui.tvView->setContextMenuPolicy(Qt::CustomContextMenu);

	connect(ui.tvView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayContextMenu(QPoint)));
	connect(ui.cbWhat, &QComboBox::currentTextChanged, this, &ParsFF::reactOnCombo);
	connect(ui.pbUpdate, &QPushButton::clicked, this, &ParsFF::reactOnUpdate);
	connect(ui.pbPrev, &QPushButton::clicked, this, &ParsFF::reactOnPrev);
	connect(ui.pbNext, &QPushButton::clicked, this, &ParsFF::reactOnNext);
	connect(ui.cbCount, &QComboBox::currentTextChanged, this, &ParsFF::reactOnCountCombo);
	connect(ui.tvView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(showDocumentPreview(QModelIndex)));
	connect(ui.tvView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(webview(QModelIndex)));
	connect(ui.tvView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(showDownloadDir(QModelIndex)));
	connect(m_player, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
	connect(ui.pbPlay, &QPushButton::clicked, this, &ParsFF::reactOnPlay);
	connect(ui.pbPause, &QPushButton::clicked, this, &ParsFF::reactOnPause);
	connect(ui.pbStop, &QPushButton::clicked, this, &ParsFF::reactOnStop);
	connect(ui.pbCancel, &QPushButton::clicked, this, &ParsFF::reactOnCancel);
	connect(parent, SIGNAL(resizeEvent(QResizeEvent)), this, SLOT(ParsFF::resizeEvent(QResizeEvent)));
}

void ParsFF::checkFileMenu()
{
	if (m_model->item(0, 0) == NULL)
	{
		m_menuActions = m_fileMenu->actions();
		for (auto it = m_menuActions.begin(); it != m_menuActions.end(); it++)
		{
			if ((*it)->text() == tools::loadStringFromResource(IDS_ACT_EXPORT_ALL) || (*it)->text() == tools::loadStringFromResource(IDS_ACT_SEARCH) || (*it)->text() == tools::loadStringFromResource(IDS_ACT_CLOSE))
			{
				(*it)->setEnabled(false);
			}
		}
	}
	else
	{
		m_menuActions = m_fileMenu->actions();
		for (auto it = m_menuActions.begin(); it != m_menuActions.end(); it++)
		{
			if ((*it)->text() == tools::loadStringFromResource(IDS_ACT_EXPORT_ALL) || (*it)->text() == tools::loadStringFromResource(IDS_ACT_SEARCH) || (*it)->text() == tools::loadStringFromResource(IDS_ACT_CLOSE))
			{
				(*it)->setEnabled(true);
			}
		}
	}
	m_menuActions.clear();
}

void ParsFF::showDownloadDir(QModelIndex path)
{
	if (ui.cbWhat->currentIndex() != Downloads)
	{
		return;
	}
	QFileDialog loads;
	QString szPath = m_model->item(path.row(), 0)->text();
	QString szFile, szBuf;
	szPath.remove("file:///");
	szPath.replace("/", "\\");
	for (auto it = szPath.rbegin(); it != szPath.rend(), *it != '\\'; it++)
	{
		szBuf += *it;
		szPath.remove(szPath.size() - 1, 1);
	}
	for (auto it = szBuf.rbegin(); it != szBuf.rend(); it++)
	{
		szFile += *it;
	}
	loads.setDirectory(szPath);	
	loads.exec();
	QStringList files = loads.selectedFiles();
	if (!files.isEmpty())
	{
		QDesktopServices::openUrl(files[0]);
	}
}

void ParsFF::reactOnCancel()
{	
	m_presenterObj->breaking();
	ui.pbNext->setEnabled(false);
}

void ParsFF::doEvents()
{
	emit resizeEvent(0);
	qApp->processEvents();
}

void ParsFF::webview(QModelIndex url)
{
	if (ui.cbWhat->currentIndex() != History)
	{
		return;
	}
	m_browser = new Web(this);
	m_browser->setUrl(m_model, url);
	m_browser->setAttribute(Qt::WA_DeleteOnClose);
	m_browser->show();
}

void ParsFF::initTreeView() 
{
	QStringList headers;
	headers << tools::loadStringFromResource(IDS_FF_USER_FOLDER);
	ui.treeWidget->setHeaderLabels(headers);	
	std::list<std::string> users = tools::getUserFolders();
	std::list <std::string>::iterator it = users.begin();
	for (it = users.begin(); it != users.end(); ++it) 
	{
		std::string current = *it;
		int pos = current.find_first_of('.');
		QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui.treeWidget);
		treeItem->setText(0, QString(current.substr(pos+1, current.size() - pos-1).c_str()));
		std::list <std::string> files = tools::getAvailableFiles(current);
		for (auto it = files.begin(); it != files.end(); ++it) 
		{
			std::string currentFile = *it;
			int slash = currentFile.find_last_of('/')+1;
			std::string buf = currentFile.substr(slash, currentFile.size() - slash);
			if (buf == "places.sqlite")
			{
				buf = tools::loadStringFromResource(IDS_HISTORY_DOWNLOADS).toStdString();
			}
			if (buf == "logins.json")
			{
				buf = tools::loadStringFromResource(IDS_LOGINS_PASSWORDS).toStdString();
			}
			if (buf == "entries")
			{
				buf = tools::loadStringFromResource(IDS_CACHE_FILES).toStdString();
			}
			QTreeWidgetItem *fileItem = new QTreeWidgetItem(treeItem);			
			fileItem->setText(0, QString(buf.c_str()));
			fileItem->setText(1, QString(currentFile.c_str()));		
		}
	}
	connect(ui.treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem *, int)));
}

void ParsFF::itemDoubleClicked(QTreeWidgetItem * item, int column)
{
	
	if (!item->parent())
	{
		return;
	}
	if (item->text(0) == tools::loadStringFromResource(IDS_HISTORY_DOWNLOADS))
	{
		m_szFileDialogPath = item->text(1);
		if (ui.cbWhat->currentIndex() != History)
		{
			ui.cbWhat->setCurrentIndex(History);
		}
		else 
		{
			reactOnCombo();
		}
	}
	if (item->text(0) == tools::loadStringFromResource(IDS_LOGINS_PASSWORDS))
	{
		m_szFileDialogPath = item->text(1);
		if (ui.cbWhat->currentIndex() != Passwords)
		{
			ui.cbWhat->setCurrentIndex(Passwords);
		}
		else 
		{ 
			reactOnCombo();
		}
	}
	if (item->text(0) == tools::loadStringFromResource(IDS_CACHE_FILES))
	{
		m_szDirDialogPath = item->text(1) + "/";
		if (ui.cbWhat->currentIndex() != Cache)
		{
			ui.cbWhat->setCurrentIndex(Cache);
		}
		else
		{ 
			reactOnCombo();
		}
	}
	return;
}


void ParsFF::addTreeRoot(QString name)
{	
	QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui.treeWidget);
	treeItem->setText(0, name);
	addTreeChild(treeItem, name);
}

void ParsFF::addTreeChild(QTreeWidgetItem *parent, QString name)
{	
	QTreeWidgetItem *treeItem = new QTreeWidgetItem();
	treeItem->setText(0, name);
	parent->addChild(treeItem);
}

void ParsFF::showDocumentPreview(QModelIndex in) 
{
	if (ui.cbWhat->currentIndex() != Cache)
	{
		return;
	}	
	std::string ext = tools::getExtensionByContentType(m_model->item(in.row(), 2)->text().toStdString());
	if (ext == "png" || ext == "jpg"|| ext == "ico" || ext == "gif")
	{
		ui.lbPic->setText(tools::loadStringFromResource(IDS_LOADING));
		ui.lbPic->repaint();
		std::string tempFile = tools::saveCacheToDisk(m_model->item(in.row(), 0)->text().toStdString(), m_model->item(in.row(), 5)->text().toStdString(), std::string("temp"));
		QImage Picture;
		bool boo = Picture.load(QString("temp"));
		ui.lbPic->setText("");
		ui.lbPic->repaint();
		if (boo)
		{
			if (Picture.width()>ui.lbPic->width())
			{
				Picture = Picture.scaledToWidth(ui.lbPic->width());
			}
			ui.lbPic->setPixmap(QPixmap::fromImage(Picture, Qt::AutoColor));
			ui.lbPic->repaint();
			return;
		}
		else 
		{
			QImage empty;
			ui.lbPic->setPixmap(QPixmap::fromImage(empty, Qt::AutoColor));
		}
	}
	if (ext == "mp3")
	{			
		if(m_player != nullptr)
		{
			delete m_player;
		}
		ui.pbPlay->setEnabled(false);
		ui.pbPause->setEnabled(false);
		ui.pbStop->setEnabled(false);
		ui.horizontalSlider->setEnabled(false);
		ui.lbMusic->setText(tools::loadStringFromResource(IDS_LOADING));
		ui.lbMusic->repaint();
		std::string tempFile = tools::saveCacheToDisk(m_model->item(in.row(), 0)->text().toStdString(), m_model->item(in.row(), 5)->text().toStdString(), std::string("tempMp3"));
		m_player = new QMediaPlayer(this);
		connect(m_player, &QMediaPlayer::mediaStatusChanged, this, &ParsFF::mediaStatusChanged);
		ui.horizontalSlider->setRange(0, m_player->duration() / 1000);
		connect(m_player, SIGNAL(durationChanged(qint64)), SLOT(durationChanged(qint64)));
		connect(m_player, SIGNAL(positionChanged(qint64)), SLOT(positionChanged(qint64)));
		connect(ui.horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(seek(int)));
		m_player->setMedia(QUrl::fromLocalFile("tempMp3"));
		m_player->setVolume(100);
		m_player->play();
		ui.lbMusic->setText(" ");
		ui.horizontalSlider->setEnabled(true);
		ui.pbPlay->setEnabled(false);
		ui.pbPause->setEnabled(true);
		ui.pbStop->setEnabled(true);
		ui.lbMusic->repaint();
		return;
	}		
	QImage empty;
	ui.lbPic->setPixmap(QPixmap::fromImage(empty, Qt::AutoColor));
	ui.lbPic->repaint();
}

void ParsFF::mediaStatusChanged(QMediaPlayer::MediaStatus status)
{	
	if (m_player->mediaStatus() == QMediaPlayer::EndOfMedia)
	{
		ui.pbPlay->setEnabled(true);
		ui.pbPause->setEnabled(false);
		ui.pbStop->setEnabled(false);
		ui.horizontalSlider->setValue(0);
		ui.horizontalSlider->setEnabled(false);
	}
}
void ParsFF::reactOnPlay()
{
	m_player->play();
	ui.horizontalSlider->setEnabled(true);
	ui.pbPlay->setEnabled(false);
	ui.pbPause->setEnabled(true);
	ui.pbStop->setEnabled(true);
}
void ParsFF::reactOnPause()
{
	m_player->pause();
	ui.pbPlay->setEnabled(true);
	ui.pbPause->setEnabled(false);
	ui.pbStop->setEnabled(true);
}
void ParsFF::reactOnStop()
{
	m_player->stop();
	delete m_player;
	m_player = nullptr;
	ui.horizontalSlider->setEnabled(false);
	ui.pbPlay->setEnabled(false);
	ui.pbPause->setEnabled(false);
	ui.pbStop->setEnabled(false);
}
void ParsFF::durationChanged(qint64 duration)
{
	ui.horizontalSlider->setMaximum(duration / 1000);
}

void ParsFF::positionChanged(qint64 progress)
{
	if (!ui.horizontalSlider->isSliderDown()) 
	{
		if (progress < m_player->duration())
		{
			ui.horizontalSlider->setValue(progress / 1000);
		}
	}	
}

void ParsFF::seek(int seconds)
{
	m_player->setPosition(seconds * 1000);
}

void ParsFF::reactOnCountCombo()
{
	m_nBookmark = 0;
	ui.pbPrev->setEnabled(false);
	reactOnCombo();
}

void ParsFF::reactOnPrev()
{		
	if (m_nBookmark > 0)
	{
		m_nBookmark -= ui.cbCount->currentText().toInt();
		if (m_nBookmark <= 0)
		{
			ui.pbPrev->setEnabled(false);
		}
	}	
	else
	{
		ui.pbPrev->setEnabled(false);
	}
	reactOnCombo();
	ui.pbNext->setEnabled(true);
}

void ParsFF::reactOnNext()
{	
	m_nBookmark += ui.cbCount->currentText().toInt();
	reactOnCombo();
	ui.pbPrev->setEnabled(true);
}

void ParsFF::close()
{
	m_model->clear();
	checkFileMenu();
	m_szFileDialogPath = "";
	m_szDirDialogPath = "";
	ui.pbNext->setEnabled(false);
}

void ParsFF::displayContextMenu(const QPoint &pos)
{	
	QMenu menu(this);
	menu.addAction(tools::loadStringFromResource(IDS_ACT_EXPORT_ALL), this, SLOT(exporting()));
	m_nRowId = ui.tvView->rowAt(pos.y());
	if ((ui.cbWhat->currentIndex() == Cache)&&(m_szDirDialogPath != "")&&(m_nRowId != -1))
	{
		QAction *A = menu.addAction(tools::loadStringFromResource(IDS_SAVE_FILE), this, SLOT(saveCacheFile()));
	}
	if (ui.prgShowStatus->isVisible())
	{
		menu.setDisabled(true);
	}
	menu.exec(ui.tvView->viewport()->mapToGlobal(pos));	
}

void ParsFF::saveCacheFile()
{
	ui.tvView->clearSelection();
	QFileDialog FD;
	FD.setAcceptMode(QFileDialog::AcceptSave);
	QStringList filters;	 
	std::string ext = tools::getExtensionByContentType(m_model->item(m_nRowId, 2)->text().toStdString());	
	if (ext != "")
	{
		filters << m_model->item(m_nRowId, 2)->text() + " (*." + QString(ext.c_str()) + ")";
		filters << tools::loadStringFromResource(IDS_ALL_FILES);
	}
	else
	{
		filters << tools::loadStringFromResource(IDS_ALL_FILES);
	}
	FD.setNameFilters(filters);	
	FD.setDirectory("/home");
	FD.exec();
	QStringList files = FD.selectedFiles();	
	std::string result = "";
	if (!files.isEmpty())		
	{
		result = tools::saveCacheToDisk(m_model->item(m_nRowId, 0)->text().toStdString(), m_model->item(m_nRowId, 5)->text().toStdString(), files[0].toStdString());
	}
	else 
	{
		return;
	}
	if (result == "Succes")
	{
		for (int i = 0; i < m_model->columnCount(); i++)
		{
			m_model->item(m_nRowId, i)->setBackground(QBrush(QColor(0, 255, 0, 100)));
		}
	}
	else 
	{
		for (int i = 0; i < m_model->columnCount(); i++)
		{
			m_model->item(m_nRowId, i)->setBackground(QBrush(QColor(255, 0, 0, 100)));
		}
	}	
	ui.tvView->clearSelection();
	ui.lbStatus->setText(result.c_str());
	ui.lbStatus->repaint();
}

void ParsFF::reactOnUpdate()
{
	reactOnCombo();
}

void ParsFF::choosingModel()
{
	switch (ui.cbWhat->currentIndex())
	{
	case Passwords:
		m_presenterObj = new PasswordsModel();
		break;
	case Downloads:
		m_presenterObj = new DownloadsModel();
		break;
	case History:
		m_presenterObj = new HistoryModel();
		break;
	case Cache:
		m_presenterObj = new CacheModel();
		break;
	default:
		throw(tools::loadStringFromResource(IDS_COMBOBOX_ERROR));
	}	
	connect(m_presenterObj, SIGNAL(proc()), this, SLOT(doEvents()));
	connect(m_presenterObj, SIGNAL(beginStatus()), this, SLOT(showBeginStatus()));
	connect(m_presenterObj, SIGNAL(middleStatus(bool)), this, SLOT(showMiddleStatus(bool)));
	connect(m_presenterObj, SIGNAL(endStatus()), this, SLOT(showEndStatus()));
	connect(m_presenterObj, SIGNAL(statusBar(bool)), this, SLOT(showStatusbar(bool)));
	connect(m_presenterObj, SIGNAL(interrupted()), this, SLOT(showInterrupting()));
}

void ParsFF::search()
{		
	ui.lbStatus->setText("");
	choosingModel();	
	if(ui.cbWhat->currentIndex() == Cache)
	{
		m_presenterObj->m_path = m_szDirDialogPath;
	}
	else 
	{
		m_presenterObj->m_path = m_szFileDialogPath;
	}		
	ui.prgShowStatus->setVisible(false);
	lockUiWhenInitView();	
	m_search = new Search(m_model, m_presenterObj, this);
	m_search->setAttribute(Qt::WA_DeleteOnClose);
	m_search->exec();
	ui.tvView->setModel(m_model);
	unlockUiWhenInitView();	
	ui.pbNext->setDisabled(true);
	ui.pbPrev->setDisabled(true);
	ui.pbUpdate->setEnabled(true);
}

void ParsFF::dirDialog()
{
	char appData[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_LOCAL_APPDATA, NULL, SHGFP_TYPE_CURRENT, appData);
	std::string path = std::string(appData) + "\\Mozilla\\Firefox\\Profiles\\";
	QString dir = QFileDialog::getExistingDirectory(this, 
		                                            tools::loadStringFromResource(IDS_OPEN_DIR_HEADER),
		                                            path.c_str(),
		                                            QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	dir.push_back('/');
	m_szDirDialogPath = dir;
	if (ui.cbWhat->currentIndex() != Cache)
	{
		ui.cbWhat->setCurrentIndex(Cache);
	}
	else
	{
		reactOnCombo();
	}
}

void ParsFF::fileDialog()
{
	QFileDialog FD;
	QStringList filters;
	filters << tools::loadStringFromResource(IDS_SQLITE_FILES) << tools::loadStringFromResource(IDS_JSON_FILES);
	FD.setNameFilters(filters);
	char appData[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, appData);
	std::string path = std::string(appData) + "\\Mozilla\\Firefox\\Profiles\\";
	FD.setDirectory(QString(path.c_str()));
	FD.exec();
	QStringList files = FD.selectedFiles();
	if (!files.empty())
	{		
		std::string file_path = files[0].toStdString();
		if (file_path.substr(file_path.find_last_of('.'), file_path.size()) == ".json")
		{
			FILE *fo = fopen(std::string(file_path.substr(0, file_path.find_last_of('/') + 1) + "key3.db").c_str(), "r");			
			if (fo == 0) 
			{
				QMessageBox msgBox;				
				msgBox.setText(QString::fromStdString("Cannot find key3.db in selected dir \"" + file_path.substr(0, file_path.find_last_of('/')+1)+ "\"" ));				
				files[0].clear();
				msgBox.exec();				
			}
			else 
			{
				fclose(fo);
			}
			ui.cbWhat->setCurrentIndex(0);
		}
		if (file_path.substr(file_path.find_last_of('.'), file_path.size()) == ".sqlite") 
		{
			ui.cbWhat->setCurrentIndex(2);
		}		
		m_szFileDialogPath = files[0];
		reactOnCombo();
		ui.pbNext->setEnabled(true);
	}
}

void ParsFF::exporting()
{
	ui.lbStatus->setText("");
	QString szPathToFile = QFileDialog::getSaveFileName(this, tools::loadStringFromResource(IDS_SAVE_FILE), "", tools::loadStringFromResource(IDS_XML_FILES));
	QFile file;
	file.setFileName(szPathToFile);
	if (file.open(QIODevice::WriteOnly))
	{
		int n = m_model->rowCount();
		int m = m_model->columnCount();
		QXmlStreamWriter stream(&file);
		stream.setAutoFormatting(true);
		stream.writeStartDocument();
		stream.writeStartElement("DATA");
		ui.prgShowStatus->setRange(0, n);
		ui.prgShowStatus->setVisible(true);
		ui.prgShowStatus->setValue(0);
		ui.prgShowStatus->repaint();
		for (int i = 0; i < n; i++)
		{
			stream.writeStartElement("ROW");
			for (int j = 0; j < m; j++)
			{
				QString head = m_model->horizontalHeaderItem(j)->text();
				QString body = m_model->item(i, j)->text();
				stream.writeTextElement(head, body);
			}
			stream.writeEndElement();		
			ui.prgShowStatus->setValue(i);
			ui.prgShowStatus->repaint();
			ui.lbStatus->setText(QString::fromStdString("Writting column: " + std::to_string(i)));
			ui.lbStatus->repaint();
		}
		ui.lbStatus->setText(tools::loadStringFromResource(IDS_WRITING_SUCCESS));
		ui.prgShowStatus->setVisible(false);
		ui.tvView->setModel(m_model);
		stream.writeEndElement();
		stream.writeEndDocument();
		file.close();
	}
}

void ParsFF::initCombo()
{
	QVector<QString> vRows= { "10","50","100","1000" };
	for (auto it = vRows.begin(); it != vRows.end(); it++)
	{
		ui.cbCount->addItem(*it);
	}
	QStringList lItems;
	lItems << CONVERTER(Passwords) << CONVERTER(Downloads) << CONVERTER(History) << CONVERTER(Cache);
	for (auto it = lItems.begin(); it != lItems.end(); it++)
	{
		ui.cbWhat->addItem(*it);
	}
	try
	{
		reactOnCombo();
	}
	catch (const std::string &str)
	{
		QMessageBox msb;
		msb.setInformativeText(QString::fromStdString(str));
	}
}

void ParsFF::reactOnCombo()
{	
	m_model->clear();
	ui.lbStatus->setText("");
	static ModelType eLastType;
	if (eLastType != (ModelType)ui.cbWhat->currentIndex())
	{
		m_nBookmark = 0;
		ui.pbPrev->setEnabled(false);
	}
	eLastType = (ModelType)ui.cbWhat->currentIndex();
	choosingModel();	
	if (ui.cbWhat->currentIndex() == Cache) 
	{
		ui.lbPic->setVisible(true);
		ui.Audio->setVisible(true);
		ui.horizontalSlider->setEnabled(false);
	}
	else 
	{
		QFile("temp").remove();
		QFile("temp.md5").remove();
		QFile("tempMp3").remove();
		QFile("tempMp3.md5").remove();
		if (m_player != nullptr)
		{
			m_player->stop();
		}
		ui.pbPlay->setEnabled(false);
		ui.pbPause->setEnabled(false);
		ui.pbStop->setEnabled(false);
		ui.Audio->setVisible(false);
		ui.lbPic->setVisible(false);
		ui.horizontalSlider->setEnabled(false);
	}
	int tablewith = ui.tvView->width() - m_vHeadWidth;
	int nCount = m_model->columnCount();
	for (int i = 0; i < nCount; i++)
	{
		ui.tvView->setColumnWidth(i, tablewith / (double)nCount);
	}	
	m_presenterObj->m_path = m_szFileDialogPath;
	m_presenterObj->m_dirPath = m_szDirDialogPath;
	ui.prgShowStatus->setVisible(false);
	lockUiWhenInitView();
	m_presenterObj->init(m_model, ui.cbCount->currentText().toInt(), m_nBookmark);
	unlockUiWhenInitView();
	emit resizeEvent(0);
	ui.tvView->setModel(m_model);
	checkFileMenu();
	if (m_nBookmark >= m_presenterObj->count() - ui.cbCount->currentText().toInt())
	{
		ui.pbNext->setEnabled(false);
	}
	else
	{
		ui.pbNext->setEnabled(true);
	}			
	ui.pbUpdate->setEnabled(false);
}

void ParsFF::lockUiWhenInitView()
{
	ui.pbNext->setEnabled(false);
	ui.cbCount->setEnabled(false);
	ui.cbWhat->setEnabled(false);
	m_fileMenu->setEnabled(false);
	ui.pbCancel->show();
	ui.pbPrev->hide();
	ui.pbNext->hide();
	ui.treeWidget->setEnabled(false);
}
void ParsFF::unlockUiWhenInitView()
{
	ui.pbNext->setEnabled(true);
	ui.cbCount->setEnabled(true);
	ui.cbWhat->setEnabled(true);
	m_fileMenu->setEnabled(true);
	ui.pbCancel->hide();
	ui.pbPrev->show();
	ui.pbNext->show();
	ui.treeWidget->setEnabled(true);
}

void ParsFF::resizeEvent(QResizeEvent *event) 
{
	int tablewith = ui.tvView->width() - m_vHeadWidth;
	int nCount = m_model->columnCount();
	for (int i = 0; i < nCount; i++)
	{
		ui.tvView->setColumnWidth(i, tablewith / double(nCount));
	}
}
void ParsFF::showBeginStatus()
{
	ui.lbStatus->setText(tools::loadStringFromResource(IDS_READING_FROM_DB));
	ui.prgShowStatus->setVisible(true);
	ui.prgShowStatus->setValue(0);
	ui.lbStatus->repaint();
	m_nIndex = 0;
}
void ParsFF::showStatusbar(const bool &bSearch)
{
	ui.prgShowStatus->setRange(0, bSearch ? m_presenterObj->count() : ui.cbCount->currentText().toInt());
	ui.lbStatus->setText("");
	ui.lbStatus->repaint();
}
void ParsFF::showInterrupting()
{
	ui.lbStatus->setText(tools::loadStringFromResource(IDS_READING_INTERRUPTED));
	ui.lbStatus->repaint();
	ui.prgShowStatus->setVisible(false);
}
void ParsFF::showMiddleStatus(const bool &bSearch)
{
	m_nIndex++;
	ui.prgShowStatus->setValue(m_nIndex);
	ui.prgShowStatus->repaint();
	ui.lbStatus->setText(QString::fromStdString(std::to_string(m_nIndex) + "/" + std::to_string(bSearch ? m_presenterObj->count() : ui.cbCount->currentText().toInt())));
	ui.lbStatus->repaint();
}
void ParsFF::showEndStatus()
{
	if (m_model->item(0,0) == NULL)
	{
		ui.lbStatus->setText(tools::loadStringFromResource(IDS_READING_FAIL));
	}
	else 
	{
		ui.lbStatus->setText(tools::loadStringFromResource(IDS_READING_SUCCESS));
	}
	ui.lbStatus->repaint();
	ui.prgShowStatus->setVisible(false);
	m_nIndex = 0;
}

ParsFF::~ParsFF()
{		
	QFile temp("temp");
	temp.remove();
	QFile tempMp3("tempMp3");
	tempMp3.remove();
	delete m_model, m_presenterObj, m_player, m_search, m_browser;
}
