//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ParsFF.rc
//
#define IDS_HEADER_URL                  101
#define IDS_READING_SUCCESS             102
#define IDS_READING_FAIL                103
#define IDS_READING_INTERRUPTED         104
#define IDS_READING_FROM_DB             105
#define IDS_HEADER_LOGIN                106
#define IDS_HEADER_PASSWORD             107
#define IDS_WRITING_SUCCESS             108
#define IDS_XML_FILES                   109
#define IDS_SAVE_FILE                   110
#define IDS_SQLITE_FILES                111
#define IDS_JSON_FILES                  112
#define IDS_OPEN_DIR_HEADER             113
#define IDS_COMBOBOX_ERROR              114
#define IDS_ALL_FILES                   115
#define IDS_ACT_EXPORT_ALL              116
#define IDS_LOADING                     117
#define IDS_CACHE_FILES                 118
#define IDS_LOGINS_PASSWORDS            119
#define IDS_HISTORY_DOWNLOADS           120
#define IDS_FF_USER_FOLDER              121
#define IDS_MENU_FILE                   122
#define IDS_ACT_OPEN                    123
#define IDS_ACT_OPEN_CACHE_FOLDER       124
#define IDS_ACT_SEARCH                  125
#define IDS_ACT_CLOSE                   126
#define IDS_HEADER_ENCODING             127
#define IDS_HEADER_TITLE                128
#define IDS_HEADER_LAST_V_DATE          129
#define IDS_HEADER_PATH                 130
#define IDS_HEADER_CONTENT              131
#define IDS_HEADER_SIZE                 132
#define IDS_HEADER_DATE_ADD             133
#define IDS_HEADER_FILEPATH             134
#define IDS_HEADER_LINK                 135
#define IDS_HEADER_TYPE                 136
#define IDS_HEADER_CREATE_DATE          137
#define IDS_HEADER_ENCODING2            138

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
