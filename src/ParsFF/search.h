﻿#ifndef SEARCH_H
#define SEARCH_H

#include <QDialog>
#include "ui_search.h"
#include "parsff.h"

class Search : public QDialog 
{
	Q_OBJECT
public:	
	Search(QStandardItemModel *model, Models *obj, QWidget * parent = 0);
	~Search();	
	void defaultSearch(QStandardItemModel *model, Models *obj, QString &szItem);
	void customSearch(QStandardItemModel *model, QString &szItem, QString &szComboText);
	void reactOnOk();	

private:
	Ui::Search ui;

	QString m_szItem;
	QStandardItemModel *m_model;
	Models *m_obj;
};
#endif // SEARCH_H