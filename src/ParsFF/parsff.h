#ifndef PARSFF_H
#define PARSFF_H

#include <QtWidgets/QWidget>
#include "ui_parsff.h"
#include <QStandardItemModel>
#include <qcombobox.h>
#include <qmediaplayer.h>
#include <models.h>
#include <passwordsmodel.h>
#include <downloadsmodel.h>
#include <historymodel.h>
#include <cachemodel.h>
#include "web.h"
#include "search.h"


#define CONVERTER(name) #name

class ParsFF : public QWidget
{
	Q_OBJECT
public:
	ParsFF(QWidget *parent = 0);
	virtual ~ParsFF();
	QString m_szFileDialogPath; //path, returned by open file dialog
	QString m_szDirDialogPath; //path, returned by open dir dialog
private:	
	QStandardItemModel *m_model;
	Models *m_presenterObj;	
	QMenu *m_fileMenu;	
	int m_nBookmark;
	int m_nRowId;
	QMediaPlayer *m_player;
	int m_nIndex;	
	class Search *m_search;
	Web *m_browser;
	const short m_vHeadWidth = 23;
	QList<QAction*> m_menuActions;

	inline void choosingModel();
	inline void lockUiWhenInitView();
	inline void unlockUiWhenInitView();
	
	void initTreeView();
	void addTreeRoot(QString name);
	void addTreeChild(QTreeWidgetItem *parent, QString name);
	int count;
	
	void checkFileMenu();
	void reactOnCombo();
	void initCombo();
	void reactOnUpdate();
	void reactOnPrev();
	void reactOnNext();
	void reactOnCountCombo();
	void reactOnPlay();
	void reactOnPause();
	void reactOnStop();
	void reactOnCancel();
	void resizeEvent(QResizeEvent *event);
	Ui::ParsFFClass ui;
private slots:
	void itemDoubleClicked(QTreeWidgetItem * item, int column);
    void showDocumentPreview(QModelIndex);
	void mediaStatusChanged(QMediaPlayer::MediaStatus status);
	void durationChanged(qint64 duration);
	void positionChanged(qint64 progress);
	void seek(int seconds);
	void saveCacheFile();
	void fileDialog();
	void dirDialog();
	void exporting();
	void search();
	void displayContextMenu(const QPoint &pos);
	void close();
	void webview(QModelIndex url);
	void doEvents();
	void showDownloadDir(QModelIndex path);
	void showBeginStatus();
	void showStatusbar(const bool &bSearch);
	void showInterrupting();
	void showMiddleStatus(const bool &bSearch);
	void showEndStatus();
	
signals:
	void cancel();

};

#endif // PARSFF_H
