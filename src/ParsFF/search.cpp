﻿#include "search.h"

Search::Search(QStandardItemModel *model, Models *obj, QWidget * parent) : QDialog(parent) 
{
	ui.setupUi(this);
	m_model = model;
	m_obj = obj;
	ui.cbColumn->addItem(" ");	
	for (int i = 0; i < model->columnCount(); i++)
	{
		ui.cbColumn->addItem(model->horizontalHeaderItem(i)->text());
	}
	connect(ui.pbOk, &QPushButton::clicked, this, &Search::reactOnOk);
	connect(ui.pbCancel, &QPushButton::clicked, this, &Search::reject);
}
void Search::reactOnOk()
{
	m_szItem = ui.leText->text();
	this->hide();
	if (ui.cbColumn->currentText() != " ")
	{
		customSearch(m_model, m_szItem, ui.cbColumn->currentText());
	}
	else
	{
		defaultSearch(m_model, m_obj, m_szItem);
	}
	this->reject();
}

void Search::defaultSearch(QStandardItemModel *model, Models *obj, QString &szItem)
{
	obj->search(model, szItem);
}

void Search::customSearch(QStandardItemModel *model, QString &szItem, QString &szComboText)
{	
	short nIndex = 0;
	for (nIndex; nIndex < model->columnCount(); ++nIndex)
	{
		if (model->headerData(nIndex, Qt::Horizontal).toString() == szComboText)
		{
			break;
		}
	}	
	int j = 0;
	for (int i = 0; i < model->rowCount(); i++)
	{
		if (model->rowCount() == j)
		{
			break;
		}
		QString body = model->item(i, nIndex)->text();
		if (body.contains(szItem))
		{
			j++;
			continue;
		}
		else
		{
			if (!model->removeRow(i))
			{
				qDebug() << "Row haven't been removed!";
			}
			i--;
		}
	}

}

Search::~Search()
{
	
}
