﻿#ifndef WEB_H
#define WEB_H

#include <QDialog>
#include "ui_web.h"
#include <QStandardItemModel>

class Web : public QDialog {
	Q_OBJECT

public:
	Web(QWidget * parent = 0);
	~Web();
	void setUrl(QStandardItemModel *model, QModelIndex &url);
private:
	Ui::Web ui;
};
#endif // WEB_H