#ifndef SQLWRAP_H
#define SQLWRAP_H

#include <iostream>

namespace wrapsql
{
	class __declspec(dllexport) WrapSQLDataBase
	{
	public:
		WrapSQLDataBase(const std::string &szDBType);		
		void setDataBaseName(const std::string &szName);
		bool open();
		void close();		
		~WrapSQLDataBase();
	};
	class __declspec(dllexport) WrapSQLQuery
	{		
	public:
		WrapSQLQuery();
		~WrapSQLQuery();
		void exec(const std::string &szQuery);
		int execCount(const std::string &szQuery);		
		bool next();
		std::string value(const unsigned int &&nIndex);
	};
}
#endif // SQLWRAP_H

