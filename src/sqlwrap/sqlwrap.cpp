// sqlwrap.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "sqlwrap.h"
#include <QtSql>
using namespace wrapsql;

QSqlDatabase db;
QSqlQuery *query;


WrapSQLDataBase::WrapSQLDataBase(const std::string &szDBType)
{
	db = QSqlDatabase::addDatabase(QString::fromStdString(szDBType));
}
void WrapSQLDataBase::setDataBaseName(const std::string &szName)
{
	db.setDatabaseName(QString::fromStdString(szName));
}
bool WrapSQLDataBase::open()
{
	if (!db.open())
		return false;
	return true;
}
void WrapSQLDataBase::close()
{
	db.close();
}

WrapSQLDataBase::~WrapSQLDataBase()
{
	close();
}
///////////////////////////////////////////////////////////////////////////////

WrapSQLQuery::WrapSQLQuery()
{
	query = new QSqlQuery(db);
}
void WrapSQLQuery::exec(const std::string &szQuery)
{
	query->exec(QString::fromStdString(szQuery));
}

int WrapSQLQuery::execCount(const std::string &szQuery)
{
	query->exec(QString::fromStdString(szQuery));
	query->next();
	int nRes = query->value(0).toInt();
	return nRes;
}

bool WrapSQLQuery::next()
{
	if (query->next())
		return true;
	return false;
}
std::string WrapSQLQuery::value(const unsigned int &&nIndex)
{
	QString szResult = query->value(nIndex).toString();
	return szResult.toStdString();
}
WrapSQLQuery::~WrapSQLQuery()
{
	delete query;
}


