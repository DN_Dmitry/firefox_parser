#ifndef DOWNLOADSMODEL_H
#define DOWNLOADSMODEL_H
#include <QObject>
#include "models.h"

class FIREFOXPARSERGUILIB_EXPORT DownloadsModel : public Models
{
	Q_OBJECT
public:
	DownloadsModel();	
	virtual ~DownloadsModel();

	void init(QStandardItemModel *model, const int &nCount, const int &nBookmark);
	virtual int count();
	void search(QStandardItemModel *model, const QString &szText);
	void breaking();
	void setHeaders(QStandardItemModel *model);
};



#endif // DOWNLOADSMODEL_H

