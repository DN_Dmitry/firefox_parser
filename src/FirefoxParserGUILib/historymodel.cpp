#include "historymodel.h"
#include <parslib.h>
#include "qdebug.h"


HistoryModel::HistoryModel()
{	
	m_libObj = new ParsLib();	
	m_nColumnCount = 3;
}
void HistoryModel::breaking()
{
	m_bBreak = true;
}

int HistoryModel::count()
{
	return m_libObj->recordsCountInHistoryTable(m_path.toStdString());
}

void HistoryModel::setHeaders(QStandardItemModel *model)
{
	model->clear();
	model->setColumnCount(m_nColumnCount);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_URL))));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_TITLE))));
	model->setHorizontalHeaderItem(2, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_LAST_V_DATE))));
}

void HistoryModel::search(QStandardItemModel *model, const QString &szText)
{
	
	if (m_path.isEmpty())
	{
		return;
	}
	model->clear();
	m_bBreak = false;
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".sqlite")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->search(m_path.toStdString(), szText.toStdString(), History)));
	int nColumn = 0;
	emit statusBar(true);
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			model->appendRow(*m_items);
			emit middleStatus(true);
			m_items->clear();
			nColumn = 0;
		}		
	}
	emit endStatus();
	delete m_lData, m_items;

}

void HistoryModel::init(QStandardItemModel *model, const int &nCount, const int &nBookmark)
{
	if (m_path.isEmpty())
	{
		return;
	}
	model->clear();
	m_bBreak = false;
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".sqlite")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->parseHistory(m_path.toStdString(), nCount, nBookmark)));
	int nColumn = 0;
	emit statusBar(false);
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{		
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			model->appendRow(*m_items);
			emit middleStatus(false);
			m_items->clear();
			nColumn = 0;
		}		
	}
	emit endStatus();
	delete m_lData, m_items;
}

HistoryModel::~HistoryModel()
{
	delete m_libObj;
}