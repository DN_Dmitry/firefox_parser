#include "passwordsmodel.h"
#include <parslib.h>

PasswordsModel::PasswordsModel()
{	
	m_nColumnCount = 3;
}
void PasswordsModel::breaking()
{
	m_bBreak = true;
}
int PasswordsModel::count()
{
	if (m_path.isEmpty())
	{
		return 0;
	}
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".json")
	{
		return 0;
	}
	qDebug(std::to_string(m_libObj->countPasswords(m_path.toStdString())).c_str() );
	return m_libObj->countPasswords(m_path.toStdString());
}
void PasswordsModel::search(QStandardItemModel *model, const QString &szText)
{
	if (m_path.isEmpty())
	{
		return;
	}
	m_bBreak = false;
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".json")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_libObj = new ParsLib();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->showDecryptedPasswords(m_path.toStdString(), 0, m_libObj->countPasswords(m_path.toStdString()))));
	delete m_libObj;
	int nColumn = 0;
	emit statusBar(false);
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			emit middleStatus(false);
			bool bOk = false;
			for (auto iter = m_items->begin(); iter != m_items->end(); iter++)
			{
				QString buf = (*iter)->text();
				if (buf.contains(szText))
				{					
					bOk = true;
					break;
				}
			}
			if (bOk == true)
			{
				model->appendRow(*m_items);				
			}			
			m_items->clear();
			nColumn = 0;
		}
	}
	emit endStatus();
	delete m_lData, m_items;
}
void PasswordsModel::setHeaders(QStandardItemModel *model)
{
	model->clear();
	model->setColumnCount(m_nColumnCount);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_URL))));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_LOGIN))));
	model->setHorizontalHeaderItem(2, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_PASSWORD))));
}

void PasswordsModel::init(QStandardItemModel *model, const int &nCount, const int &nBookmark)
{	
	if (m_path.isEmpty())
	{
		return;
	}
	m_bBreak = false;
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".json")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_libObj = new ParsLib();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->showDecryptedPasswords(m_path.toStdString(), nBookmark, nCount)));
	delete m_libObj;	
	int nColumn = 0;
	emit statusBar(false);
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{	
		emit proc();
		
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			emit middleStatus(false);
			model->appendRow(*m_items);
			m_items->clear();
			nColumn = 0;
		}		
	}	
	emit endStatus();
	delete m_lData, m_items;
}

PasswordsModel::~PasswordsModel()
{
	
}