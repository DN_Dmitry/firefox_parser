#ifndef CACHEMODEL_H
#define CACHEMODEL_H

#include "models.h"
class FIREFOXPARSERGUILIB_EXPORT CacheModel : public Models
{
	Q_OBJECT
public:
	CacheModel();

	virtual ~CacheModel();

	void init(QStandardItemModel *model, const int &nCount, const int &nBookmark);
	virtual int count();
	void search(QStandardItemModel *model, const QString &szText);
	void breaking();
	void setHeaders(QStandardItemModel *model);
};


#endif // CACHEMODEL_H

