#include "models.h"

Models::Models()
{

}

Models::~Models()
{

}

QString Models::loadStringFromResource(unsigned int id)
{
	HINSTANCE hInstance = NULL;
	TCHAR szBuffer[128];
	LoadString(hInstance, id, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
	QString szResult = QString::fromWCharArray(szBuffer);
	return szResult;
}