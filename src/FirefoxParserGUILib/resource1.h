//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FirefoxParserGUILib1.rc
//
#define IDS_HEADER_URL                  101
#define IDS_HEADER_LOGIN                106
#define IDS_HEADER_PASSWORD             107
#define IDS_HEADER_ENCODING             127
#define IDS_HEADER_TITLE                128
#define IDS_HEADER_LAST_V_DATE          129
#define IDS_HEADER_PATH                 130
#define IDS_HEADER_CONTENT              131
#define IDS_HEADER_SIZE                 132
#define IDS_HEADER_DATE_ADD             133
#define IDS_HEADER_FILEPATH             134
#define IDS_HEADER_LINK                 135
#define IDS_HEADER_TYPE                 136
#define IDS_HEADER_CREATE_DATE          137
#define IDS_HEADER_ENCODING2            138

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
