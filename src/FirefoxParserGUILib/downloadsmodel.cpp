#include "downloadsmodel.h"
#include <parslib.h>

DownloadsModel::DownloadsModel()
{	
	m_libObj = new ParsLib();	
	m_nColumnCount = 4;
}
void DownloadsModel::breaking()
{
	m_bBreak = true;
}

int DownloadsModel::count()
{
	return m_libObj->recordsCountInDownloadsTable(m_path.toStdString());
}

void DownloadsModel::setHeaders(QStandardItemModel *model)
{
	model->clear();
	model->setColumnCount(m_nColumnCount);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_PATH))));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_CONTENT))));
	model->setHorizontalHeaderItem(2, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_SIZE))));
	model->setHorizontalHeaderItem(3, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_DATE_ADD))));
}

void DownloadsModel::search(QStandardItemModel *model, const QString &szText)
{	
	if (m_path.isEmpty())
	{
		return;
	}
	model->clear();
	m_bBreak = false;
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".sqlite")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->search(m_path.toStdString(), szText.toStdString(), Downloads)));
	emit statusBar(true);	
	int nColumn = 0;
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			model->appendRow(*m_items);
			emit middleStatus(true);
			m_items->clear();
			nColumn = 0;
		}		
	}
	emit endStatus();
	delete m_lData, m_items;
}
void DownloadsModel::init(QStandardItemModel *model, const int &nCount, const int &nBookmark)
{
	if (m_path.isEmpty())
	{
		return;
	}
	m_bBreak = false;
	model->clear();
	if (m_path.toStdString().substr(m_path.toStdString().find_last_of('.'), m_path.size()) != ".sqlite")
	{
		return;
	}
	setHeaders(model);
	emit beginStatus();
	m_lData = new QList<std::string>(QList<std::string>::fromStdList(m_libObj->parseDownloads(m_path.toStdString(), nCount, nBookmark)));
	int nColumn = 0;
	emit statusBar(false);
	m_items = new QList<QStandardItem*>();
	for (auto it = m_lData->begin(); it != m_lData->end(); it++)
	{			
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		m_items->append(new QStandardItem(QString::fromStdString(*it)));
		nColumn++;
		if (m_nColumnCount == nColumn)
		{
			model->appendRow(*m_items);
			emit middleStatus(false);
			m_items->clear();
			nColumn = 0;
		}		
	}	
	emit endStatus();
	delete m_lData, m_items;

}

DownloadsModel::~DownloadsModel()
{
	delete m_libObj;
}