#ifndef PASSWORDSMODEL_H
#define PASSWORDSMODEL_H
#include <QObject>
#include "models.h"

class FIREFOXPARSERGUILIB_EXPORT PasswordsModel : public Models
{
	Q_OBJECT
public:
	PasswordsModel();
	virtual ~PasswordsModel();
	void init(QStandardItemModel *model, const int &nCount, const int &nBookmark);
	virtual int count();
	void search(QStandardItemModel *model, const QString &szText);
	void breaking();
	void setHeaders(QStandardItemModel *model);
};


#endif // PASSWORDSMODEL_H
