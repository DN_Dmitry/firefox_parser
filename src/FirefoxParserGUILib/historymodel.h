#ifndef HISTORYMODEL_H
#define HISTORYMODEL_H
#include <QObject>
#include "models.h"

class FIREFOXPARSERGUILIB_EXPORT HistoryModel : public Models
{
	Q_OBJECT
public:
	HistoryModel();
	virtual ~HistoryModel();
	void init(QStandardItemModel *model, const int &nCount, const int &nBookmark);
	virtual int count();
	void search(QStandardItemModel *model, const QString &szText);
	void breaking();
	void setHeaders(QStandardItemModel *model);
};


#endif // HISTORYMODEL_H

