#ifndef MODELS_H
#define MODELS_H

#include <QObject>
#include <QStandardItemModel>
#include "firefoxparserguilib_global.h"
#include <parslib.h>
#include "resource1.h"

class FIREFOXPARSERGUILIB_EXPORT Models : public QObject
{
	Q_OBJECT
public:
	Models();
	~Models();	

	int m_nColumnCount;
	QList<QStandardItem*> *m_items;
	QList<std::string> *m_lData;
	ParsLib *m_libObj;
	bool m_bBreak;
	QString m_path;
	QString m_dirPath;	

	virtual void init(QStandardItemModel *model, const int &nCount, const int &nBookmark) = 0;
	virtual int count() = 0;
	virtual void search(QStandardItemModel *model, const QString &szText) = 0;
	virtual void breaking() = 0;
	virtual void setHeaders(QStandardItemModel *model) = 0;
	virtual QString loadStringFromResource(unsigned int id);
signals:
	void proc();
	void beginStatus();
	void middleStatus(bool);
	void endStatus();
	void statusBar(bool);
	void interrupted();
	
};

#endif // MODELS_H
