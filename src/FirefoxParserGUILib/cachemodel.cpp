#include "cachemodel.h"
#include <parslib.h>
#include <fstream>
#include <QDebug>

CacheModel::CacheModel()
{
	m_libObj = new ParsLib();	
	m_nColumnCount = 6;
}
int CacheModel::count()
{
	return m_libObj->m_vszCachePathList.size();
}

void CacheModel::breaking()
{
	m_bBreak = true;
}

void CacheModel::setHeaders(QStandardItemModel *model)
{
	model->clear();
	model->setColumnCount(m_nColumnCount);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_FILEPATH))));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_LINK))));
	model->setHorizontalHeaderItem(2, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_TYPE))));
	model->setHorizontalHeaderItem(3, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_CREATE_DATE))));
	model->setHorizontalHeaderItem(4, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_ENCODING))));
	model->setHorizontalHeaderItem(5, new QStandardItem(QString(loadStringFromResource(IDS_HEADER_SIZE))));
}

void CacheModel::init(QStandardItemModel *model, const int &nCount, const int &nBookmark)
{
	if (m_dirPath.isEmpty())
	{
		return;
	}
	m_bBreak = false;
	model->clear();
	setHeaders(model);
	emit beginStatus();	
	if (m_libObj->m_vszCachePathList.empty())
	{
		m_libObj->initFileList(m_dirPath.toStdString());
	}
	std::vector<std::string> pathes = m_libObj->getPathList(nBookmark, nCount);
	emit statusBar(false);
	m_items = new QList<QStandardItem*>();
	std::list<std::string> row;	
	for (std::vector<std::string>::iterator qq = pathes.begin(); qq != pathes.end(); ++qq)
	{		
		row = m_libObj->getFilDataByCacheFilePath(*qq);
		for (std::list<std::string>::iterator it = row.begin(); it != row.end(); ++it)
		{
			m_items->append(new QStandardItem(QString::fromStdString(*it)));
		}
		emit proc();		
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();			
			return;
		}	   
		emit middleStatus(false);		
		model->appendRow(*m_items);
		m_items->clear();
	}
	emit endStatus();
	delete m_items;
}

void CacheModel::search(QStandardItemModel *model, const QString &szText)
{
	if (m_path.isEmpty())
	{
		return;
	}
	m_bBreak = false;
	model->clear();
	setHeaders(model);
	emit beginStatus();	
	if (m_libObj->m_vszCachePathList.empty())
	{
		m_libObj->initFileList(m_path.toStdString());
	}
	std::vector<std::string> pathes = m_libObj->getPathList(0, 0);
	emit statusBar(true);
	m_items = new QList<QStandardItem*>();
	std::list<std::string> row;
	for (auto it = pathes.begin(); it != pathes.end(); ++it)
	{		
		row = m_libObj->getFilDataByCacheFilePath(*it);
		auto iter = row.begin();
		std::advance(iter, 2);		
		emit proc();
		if (m_bBreak)
		{
			m_items->clear();
			delete m_items;
			emit interrupted();
			return;
		}
		if (std::string(*iter).find(szText.toStdString()) == -1)
		{
			emit middleStatus(true);
			continue;
		}
		for (iter = row.begin(); iter != row.end(); ++iter)
		{
			m_items->append(new QStandardItem(QString::fromStdString(*iter)));
		}		
		emit middleStatus(true);
		model->appendRow(*m_items);
		m_items->clear();
	}
	emit endStatus();
	delete m_items;
}

CacheModel::~CacheModel()
{
	delete m_libObj;
}