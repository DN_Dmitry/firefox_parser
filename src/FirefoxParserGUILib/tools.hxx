#ifndef TOOLS_HXX
#define TOOLS_HXX

#include <parslib.h>
namespace tools
{
	ParsLib nsLibrary;	
	std::list<std::string> getUserFolders()
	{
		return nsLibrary.getUserFolders();
	}
	std::list<std::string> getAvailableFiles(std::string name)
	{
		return nsLibrary.getAvailableFiles(name);
	}
	std::string	getExtensionByContentType(const std::string &name)
	{
		return nsLibrary.getExtensionByContentType(name);
	}
	std::string saveCacheToDisk(const std::string &in, std::string &size, std::string &out)
	{
		return nsLibrary.saveCacheToDisk(in, size, out);
	}
	QString loadStringFromResource(unsigned int id)
	{
		HINSTANCE hInstance = NULL;
		TCHAR szBuffer[128];
		LoadString(hInstance, id, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
		QString szResult = QString::fromWCharArray(szBuffer);
		return szResult;
	}
}
#endif // TOOLS_HXX