#define _CRT_SECURE_NO_WARNINGS

#include "parslib.h"
#include <sqlwrap.h>
#include <fstream>
#include <sstream>

using namespace wrapsql;

ParsLib::ParsLib()
{	
}

char* ParsLib::dupncat(const char *s1, unsigned int n)
{
	char *p, *q;
	p = new char[n + 1];
	q = p;
	for (unsigned int i = 0; i < n; i++) 
	{
		strcpy(q + i, s1);
	}
	return p;
}

char* ParsLib::md5(const char *data, int size)
{
	HCRYPTHASH hHash;
	HCRYPTPROV hProv;
	BYTE md5hash[16];
	DWORD md5hash_size, dwSize;
	static char str_hash[33];
	ZeroMemory(str_hash, sizeof(str_hash));
	ZeroMemory(md5hash, sizeof(md5hash));
	CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, 0);
	CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash);
	CryptHashData(hHash, (BYTE*)data, size, 0);
	dwSize = sizeof(md5hash_size);
	CryptGetHashParam(hHash, HP_HASHSIZE, (BYTE *)&md5hash_size, &dwSize, 0);
	CryptGetHashParam(hHash, HP_HASHVAL, md5hash, &md5hash_size, 0);
	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	for (unsigned int i = 0; i < md5hash_size; i++)
	{
		sprintf(str_hash + 2 * i, "%2.2x", md5hash[i]);
	}
	return str_hash;
}


BOOL  ParsLib::loadFunctions() 
{
	const char *path = getenv("PATH");
	if (path == NULL)
	{
		return 1;
	}	
	std::string newpath("PATH=" + std::string(path) + ";C:\\Program Files (x86)\\Mozilla Firefox");	
	if (_putenv(newpath.c_str()) < 0)
	{
		return 1;
	}
	HMODULE hNSS = LoadLibraryA("C:\\Program Files (x86)\\Mozilla Firefox\\nss3.dll"); 
	if (hNSS)
	{    
		fpNSS_INIT = (NSS_Init)GetProcAddress(hNSS, "NSS_Init");
		fpNSS_Shutdown = (NSS_Shutdown)GetProcAddress(hNSS, "NSS_Shutdown");
		PK11GetInternalKeySlot = (PK11_GetInternalKeySlot)GetProcAddress(hNSS, "PK11_GetInternalKeySlot");
		PK11FreeSlot = (PK11_FreeSlot)GetProcAddress(hNSS, "PK11_FreeSlot");
		PK11Authenticate = (PK11_Authenticate)GetProcAddress(hNSS, "PK11_Authenticate");
		PK11SDRDecrypt = (PK11SDR_Decrypt)GetProcAddress(hNSS, "PK11SDR_Decrypt");
	}
	return !(!fpNSS_INIT || !fpNSS_Shutdown || !PK11GetInternalKeySlot || !PK11Authenticate || !PK11SDRDecrypt || !PK11FreeSlot);

}
char* ParsLib::crack(const char *s) 
{
	BYTE byteData[8096];
	DWORD dwLength = 8096;
	PK11SlotInfo *slot = 0;
	SECStatus status;
	SECItem in, out;
	char *result = "";
	ZeroMemory(byteData, sizeof(byteData));
	if (CryptStringToBinaryA(s, strlen(s), CRYPT_STRING_BASE64, byteData, &dwLength, 0, 0)) 
	{
		slot = (*PK11GetInternalKeySlot) ();
		if (slot != NULL) 
		{
			status = PK11Authenticate(slot, true, NULL);
			if (status == SECSuccess) 
			{
				in.data = byteData;
				in.len = dwLength;
				out.data = 0;
				out.len = 0;
				status = (*PK11SDRDecrypt) (&in, &out, NULL);
				if (status == SECSuccess) 
				{
					memcpy(byteData, out.data, out.len);
					byteData[out.len] = 0;
					result = ((char*)byteData);
				}
				else
				{
					result = "Error on decryption!";
				}
			}
			else
			{
				result = "Error on authenticate!";
			}
			(*PK11FreeSlot) (slot);
		}
		else 
		{
			result = "Get Internal Slot error!";
		}
	}
	return result;
}
char* ParsLib::dupcat(const char *s1, ...)
{
	int len;
	char *p, *q, *sn;
	va_list ap;
	len = strlen(s1);
	va_start(ap, s1);
	while (1)
	{
		sn = va_arg(ap, char *);
		if (!sn)
			break;
		len += strlen(sn);
	}
	va_end(ap);
	p = new char[len + 1];
	strcpy(p, s1);
	q = p + strlen(p);
	va_start(ap, s1);
	while (1) 
	{
		sn = va_arg(ap, char *);
		if (!sn)
		{ 
			break;
		}
		strcpy(q, sn);
		q += strlen(q);
	}
	va_end(ap);
	return p;
}

std::list<std::string> ParsLib::showDecryptedPasswords(const std::string &name, int start, int n)
{		
	m_lBuffer.clear();
	if (loadFunctions()) 
	{					
		std::string workdir = name.substr(0, name.find_last_of('/'));
		if (!(*fpNSS_INIT) (workdir.c_str()))
		{
			FILE *loginJson;
			DWORD JsonFileSize = 0;
			char *p, *q, *qu;
			int entries = 0;
			if (name.empty())
			{
				return m_lBuffer;
			}
			else
			{ 
				loginJson = fopen(name.c_str(), "r");
			}
			if (loginJson)
			{
				fseek(loginJson, 0, SEEK_END);
				JsonFileSize = ftell(loginJson);
				fseek(loginJson, 0, SEEK_SET);
				p = new char[JsonFileSize + 1];
				fread(p, 1, JsonFileSize, loginJson);
				int ind = 0;
				while ((q = strstr(p, "formSubmitURL")) != NULL)
				{					
					q += strlen("formSubmitURL") + 3;
					qu = strstr(q, "usernameField") - 3;
					*qu = '\0';					
					if (((ind >= start) && (ind < start + n)))
					{
						m_lBuffer.push_back(q);
					}
					q = strstr(qu + 1, "encryptedUsername") + strlen("encryptedUsername") + 3;
					qu = strstr(q, "encryptedPassword") - 3;
					*qu = '\0';
					if (((ind >= start) && (ind < start + n)))
					{
						m_lBuffer.push_back(crack(q));
					}
					q = strstr(qu + 1, "encryptedPassword") + strlen("encryptedPassword") + 3;
					qu = strstr(q, "guid") - 3;
					*qu = '\0';
					p = qu + 1;
					if (((ind >= start) && (ind < start + n)))
					{
						m_lBuffer.push_back(crack(q));
					}
					ind++;
					p = qu + 1;
				}
				fclose(loginJson);
			}
			(*fpNSS_Shutdown) ();
		}
	}
	return m_lBuffer;
}

int ParsLib::countPasswords(const std::string &name) 
{
	int n;
	FILE *loginJson;
	DWORD JsonFileSize = 0;	
	int entries = 0;
	if (name.empty())
	{
		return 0;
	}
	else
	{
		loginJson = fopen(name.c_str(), "r");
	}
	std::string buffer = "";
	char c = 0;
	while (1) 
	{
		fread(&c, sizeof(char), 1, loginJson);
		if (c == '[')
		{
			break;
		}
		buffer += c;
	}

	int pos = buffer.find("nextId");
	buffer = buffer.substr(pos + strlen("nextId") + 2, buffer.size() - pos - strlen("nextId") - 2);
	buffer = buffer.substr(0, buffer.find(","));
	std::istringstream ss(buffer);	
	ss >> n;
	return n-1;
}

std::list<std::string> ParsLib::search(const std::string &name, const std::string &szText, const ModelType &eType)
{	
	WrapSQLDataBase *db = new WrapSQLDataBase("QSQLITE");
	if (!name.empty())
	{
		db->setDataBaseName(name);
	}
	else
	{
		return m_lBuffer;
	}
	if (!db->open())
	{
		throw new std::exception("Can't open db");
	}	
	if (eType == History)
	{
		WrapSQLQuery *query = new WrapSQLQuery();
		query->exec("SELECT url, title, datetime(last_visit_date/1000000,'unixepoch')AS last_visit_date FROM moz_places WHERE title LIKE '%" + szText + "%'");
		while (query->next())
		{
			m_lBuffer.push_back(query->value(0));
			m_lBuffer.push_back(query->value(1));
			m_lBuffer.push_back(query->value(2));
		}
		db->close();
		delete query, db;
		return m_lBuffer;
	}
	else(eType == Downloads);
	{
		WrapSQLQuery *queryContent = new WrapSQLQuery;
		WrapSQLQuery *queryDate = new WrapSQLQuery;
		std::vector<std::string> lContent;
		queryContent->exec("SELECT content FROM moz_annos");
		while (queryContent->next())
		{
			lContent.push_back(queryContent->value(0));
		}
		if (lContent.size() == 0)
		{
			return m_lBuffer;
		}
		queryDate->exec("SELECT datetime(dateAdded/1000000,'unixepoch')AS dateAdded FROM moz_annos");
		unsigned int n = 0;
		while (queryDate->next())
		{
			if (n > lContent.size() - 2)
			{
				break;
			}
			if (lContent[n + 1].find(szText) == -1)
			{				
				n += 3;
				continue;
			}				
			std::string str = queryDate->value(0);
			m_lBuffer.push_back(lContent[n]);
			m_lBuffer.push_back(lContent[n + 1]);

			int nIndex = lContent[n + 2].find("fileSize");
			if (nIndex == -1)
			{
				lContent[n + 2] = "NULL";
			}
			else
			{
				lContent[n + 2].erase(0, nIndex + 10);
				nIndex = lContent[n + 2].length() - 1;
				lContent[n + 2].erase(nIndex);
			}
			m_lBuffer.push_back(lContent[n + 2]);
			m_lBuffer.push_back(queryDate->value(0));
			n += 3;
			if (n > lContent.size() - 2)
			{
				break;
			}
		}
		db->close();
		lContent.clear();	

		delete queryContent, queryDate, db;
		return m_lBuffer;
	}
}

std::list<std::string> ParsLib::parseHistory(const std::string &name, const int &nCount, const int &nBookmark)
{	
	WrapSQLDataBase db("QSQLITE");
	if (!name.empty())
	{
		db.setDataBaseName(name);
	}
	else
	{
		return m_lBuffer;
	}
	if (!db.open())
	{
		throw new std::exception("Can't open db");
	}
	WrapSQLQuery query;	
	query.exec("SELECT url, title, datetime(last_visit_date/1000000,'unixepoch')AS last_visit_date FROM moz_places LIMIT " + std::to_string(nBookmark) + ", " + std::to_string(nCount));
	while (query.next())
	{
		m_lBuffer.push_back(query.value(0));
		m_lBuffer.push_back(query.value(1));
		m_lBuffer.push_back(query.value(2));
	}
	db.close();
	return m_lBuffer;
}

std::list<std::string> ParsLib::parseDownloads(const std::string &name, const int &nCount, const int &nBookmark)
{
	WrapSQLDataBase db("QSQLITE");
	if (!name.empty())
	{
		db.setDataBaseName(name);
	}
	else
	{
		return m_lBuffer;
	}
	if (!db.open())
	{
		throw new std::exception("Can't open db");
	}
	WrapSQLQuery *queryContent = new WrapSQLQuery();
	WrapSQLQuery *queryDate = new WrapSQLQuery();
	WrapSQLQuery *place_id = new WrapSQLQuery();
	std::vector<std::string> lContent;
	std::vector<std::string> lPlace_id;
	std::vector<std::string> lData;
	queryContent->exec("SELECT content FROM moz_annos ORDER BY place_id  LIMIT " + std::to_string(nBookmark*3) + ", " + std::to_string(nCount*3));
	while (queryContent->next())
	{
		lContent.push_back(queryContent->value(0));
	}
	queryDate->exec("SELECT datetime(dateAdded/1000000,'unixepoch')AS dateAdded FROM moz_annos ORDER BY place_id LIMIT " + std::to_string(nBookmark*3) + ", " + std::to_string(nCount*3));
	while (queryDate->next())
	{
		lData.push_back(queryDate->value(0));
	}
	place_id->exec("SELECT place_id FROM moz_annos ORDER BY place_id LIMIT " + std::to_string(nBookmark *3) + ", " + std::to_string(nCount * 3));
	while (place_id->next())
	{
		lPlace_id.push_back(place_id->value(0));
	}
	int tempId = 0;
	int id = 0;
	for (unsigned int i = 0; i < lContent.size()-2;i++)
	{
		std::string str(lPlace_id[i]);
		std::istringstream ss(str);
		ss >> id;		
		std::vector<std::string> temp;
		bool flag = 0;
		for (int j = 0; j < 3; j++) 
		{
			std::string buf(lPlace_id[i+j]);
			std::istringstream ssbuf(buf);
			ssbuf >> tempId;
			if (tempId != id)
			{
				flag = 1;
			}
			temp.push_back(lContent[i + j]);		
		}
		if (flag == 0) 
		{		
			m_lBuffer.push_back(temp[0]);
			m_lBuffer.push_back(temp[1]);
			int pos= temp[2].find("fileSize") + strlen("fileSize") + 2;
			int SizeOfData = 0;
			if (pos - strlen("fileSize") - 2 != -1) //))
			{
				std::string bufer = temp[2].substr(pos, temp[2].size() - pos);
				bufer.erase(bufer.size() - 1, 1);
				m_lBuffer.push_back(bufer);
			}
			m_lBuffer.push_back(lData[i]);
		}
			
	}	
	delete queryContent, queryDate, place_id;
	return m_lBuffer;
}

//need to load all pathes in memory becouse cannot paging. the function FindNextFileA is not determine
void ParsLib::initFileList(const std::string &name)
{ 
	m_vszCachePathList = std::vector<std::string>();
	std::string path = name + "//*";
	WIN32_FIND_DATAA FindFileData;
	HANDLE hf = FindFirstFileA(path.c_str(), &FindFileData);
	if (hf != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (!(FindFileData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
			{
				m_vszCachePathList.push_back(name + std::string(FindFileData.cFileName));
			}
		} while (FindNextFileA(hf, &FindFileData));
	    FindClose(hf);
	}
}

//getting pathes for currert page in gui
std::vector<std::string> ParsLib::getPathList(unsigned int startpos, unsigned int n)
{
	std::vector<std::string> vszResult;
	if (n == 0)
	{
		vszResult = m_vszCachePathList;
		return vszResult;
	}
	for (unsigned int i = startpos; ((i < startpos + n)&& (i < m_vszCachePathList.size())); i++)
	{		
		vszResult.push_back(m_vszCachePathList[i]);
	}
	return vszResult;
}

//need to add a lot of types
std::string ParsLib::getExtensionByContentType(const std::string &name) 
{	
	if (name == "image/gif")
	{
		return "gif";
	}
	if (name == "image/png")
	{
		return "png";
	}
	if (name == "image/jpeg")
	{
		return "jpg";
	}
	if (name == "image/x-icon")
	{
		return "ico";
	}
	if (name == "image/ico")
	{
		return "ico";
	}
	if (name == "video/mp4")
	{
		return "mp4";
	}
	if (name == "audio/mpeg")
	{
		return "mp3";
	}
	if (name == "audio/webm")
	{
		return "webm";
	}
	return "";
}
void ParsLib::removeCacheFromDisk(const char* name)
{
	DeleteFile((LPCWSTR)name);
}
std::string ParsLib::getDataArray(const std::string &name, const std::string &size) 
{
	if (name.empty())
	{
		return "No input path!";
	}
	if (size == "-")
	{
		return "No size!";
	}
	std::string result = "";
	std::ifstream input(name.c_str(), std::ifstream::in | std::ifstream::binary);
	if (!input.is_open())
	{
		return "Error open output file";
	}
	int DataSize = 0;
	std::istringstream ist(size);
	ist >> DataSize;
	if (DataSize > 0)
	{
		char c = 0;
		for (int i = 0; i < DataSize; i++)
		{
			c = input.get();
			result.push_back(c);
		}
	}
	else
	{
		return "Size is invalid!";
	}
	input.close();
	return result;
}
//reading data of current file and save it to user`s folder 
std::string ParsLib::saveCacheToDisk(const std::string &in, std::string &size, std::string &out) {

	if (in.empty())
	{
		return std::string("No input path!");
	}
	if (size == "-")
	{
		return std::string("No size!");
	}
	if (out == "-")
	{
		return std::string("No destination path!");
	}
	//////////////////////////////////////////////////////TODO///////////////////////////////////////////////
	std::ifstream input(in.c_str(), std::ifstream::in | std::ifstream::binary);
	if (!input.is_open())
	{
		return std::string("Error open output file");
	}
	std::string fileData = "";
	int DataSize = 0;
	std::istringstream ist(size);
	ist >> DataSize;
	if (DataSize > 0) 
	{
		std::ofstream output(out.c_str(), std::ifstream::out | std::ifstream::binary);
		if (!input.is_open())
		{
			return std::string("Error open output file");
		}
		char c = 0;
		for (int i = 0; i < DataSize; i++) 
		{
			c = input.get();
			fileData += c;
			output << c;
		}
		output.close();

	}
	else
	{
		return "Size is invalid!";
	}
	std::ofstream md5_out((out + ".md5").c_str(), std::ifstream::out | std::ifstream::binary);
	if (!md5_out.is_open())
	{
		return std::string("Error open output file");
	}
	md5_out << md5(fileData.c_str(), fileData.size());
	md5_out.close();
	input.close();
	return "Succes";
}

//current file parsing
std::list<std::string> ParsLib::getFilDataByCacheFilePath(const std::string &name) 
{	
	std::list <std::string> result;
	int DataSize = 0;	
	std::list <std::string> BADresult;
	BADresult.push_back(name);
	for (int i = 0; i < 5; i++)
	{
		BADresult.push_back("-");
	}
	if (name.empty())
	{
		return BADresult;
	}	
	FILE *input = fopen(name.c_str(), "rb+");
	if (input == 0) 
	{		
		return BADresult;
	}
	std::vector <std::string> headerStrings;
	std::string buffer = "";
	fseek(input, 0, SEEK_END);
	int size = ftell(input);
	char c = 0;
	bool headerEndFlag = 1;
	int ind = 0;
	while (1) 
	{
		if (ind > 10000)
		{
			return BADresult; //wrong file
		}
		fseek(input, (-2)*(int)sizeof(char), SEEK_CUR); 
		if (ftell(input) == 0)
		{
			headerStrings.push_back(buffer);
			buffer.clear();
			break;
		}
		fread(&c, sizeof(char), 1, input);
		if ((c == '\n') || (c == '\r'))
		{
			if (headerEndFlag)
			{
				headerStrings.push_back(buffer);
			}
			if (buffer.find("sredaeh-esnopser-lanigiro") != std::string::npos)
			{
				headerEndFlag = 0;
			}
			if (buffer.find("okcen") != std::string::npos)
			{
				headerStrings.push_back(buffer);
				break;
			}
			buffer.clear();
		}
		buffer += c;
		ind++;
	}
	buffer.clear();
	result.push_back(name);
	for (unsigned int x = 0; x < headerStrings.size(); x++)
	{
		for (unsigned int i = 0; i < headerStrings[x].size(); i++)
		{
			if (headerStrings[x][i] == '\r')
			{
				headerStrings[x].erase(i, 1);
			}
		}
	}	
	std::reverse(headerStrings[headerStrings.size() - 1].begin(), headerStrings[headerStrings.size() - 1].end());
	int headerStartPos = headerStrings[headerStrings.size() - 1].find("necko:classified"); //necko:classified
	if (headerStartPos != std::string::npos)
	{
		for (int i = headerStartPos - 2; i > 0; i--) 
		{
			if ((headerStrings[headerStrings.size() - 1][i] <= 32) || (headerStrings[headerStrings.size() - 1][i] >= 126))
			{
				break;
			}
			buffer += headerStrings[headerStrings.size() - 1][i];
		}
		std::reverse(buffer.begin(), buffer.end());
		buffer.erase(0, 1);
		result.push_back(buffer);
	}
	else
	{
		result.push_back("-");
	}
	buffer.clear();
	for (unsigned int x = 0; x < headerStrings.size(); x++)
	{
		headerStartPos = headerStrings[x].find(":epyT-tnetnoC"); //Content-Type:
		if (headerStartPos != std::string::npos)
		{
			for (int i = 0; i < headerStartPos-1; i++) 
			{
				buffer += headerStrings[x][i];
			}
			std::reverse(buffer.begin(), buffer.end());
			result.push_back(buffer);
			break;
		}
		if (x == headerStrings.size() - 1)
		{
			result.push_back("-");
		}
	}
	buffer.clear();
	for (unsigned int x = 0; x < headerStrings.size(); x++)
	{
		headerStartPos = headerStrings[x].find(":etaD"); //Date
		if (headerStartPos != std::string::npos)
		{
			for (int i = 0; i < headerStartPos; i++) 
			{
				buffer += headerStrings[x][i];
			}
			std::reverse(buffer.begin(), buffer.end());
			result.push_back(buffer);
			break;
		}
		if (x == headerStrings.size() - 1)
		{
			result.push_back("-");
		}		
	}
	buffer.clear();
	for (unsigned int x = 0; x < headerStrings.size(); x++)
	{
		headerStartPos = headerStrings[x].find(":gnidocnE-tnetnoC"); //Content-Encoding:
		if (headerStartPos != std::string::npos) 
		{
			for (int i = 0; i < headerStartPos; i++) 
			{
				buffer += headerStrings[x][i];
			}
			std::reverse(buffer.begin(), buffer.end());
			result.push_back(buffer);
			break;
		}
		if (x == headerStrings.size() - 1)
		{
			result.push_back("none");
		}
	}
	buffer.clear();
	for (unsigned int x = 0; x < headerStrings.size(); x++)
	{
		headerStartPos = headerStrings[x].find(":htgneL-tnetnoC");
		if (headerStartPos != std::string::npos)
		{
			for (int i = 0; i < headerStartPos; i++)
			{
				buffer += headerStrings[x][i];
			}
			std::reverse(buffer.begin(), buffer.end());
			result.push_back(buffer);			
			break;
		}
		if (x == headerStrings.size() - 1)
		{
			result.push_back("-");
		}

	}
	buffer.clear();	
	fclose(input);
	return result;
}

int ParsLib::recordsCountInHistoryTable(const std::string &name)
{
	WrapSQLDataBase db("QSQLITE");
	if (!name.empty())
	{
		db.setDataBaseName(name);
	}
	else
	{
		return 0;
	}
	if (!db.open())
	{
		throw new std::exception("Can't open db");
	}
	WrapSQLQuery query;
	m_nRCount = query.execCount("SELECT COUNT(*) FROM moz_places");
	db.close();
	return m_nRCount;
}

int ParsLib::recordsCountInDownloadsTable(const std::string &name)
{
	WrapSQLDataBase db("QSQLITE");
	if (!name.empty())
	{
		db.setDataBaseName(name);
	}
	else
	{
		return 0;
	}
	if (!db.open())
	{
		throw new std::exception("Can't open db");
	}
	WrapSQLQuery query;
	m_nRCount = query.execCount("SELECT COUNT(*) FROM moz_annos") / 3;
	db.close();
	return m_nRCount;
}

std::list <std::string> ParsLib::getUserFolders()
{
	std::list <std::string> result;	
	char appData[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, appData);
	std::string path = std::string(appData) + "\\Mozilla\\Firefox\\profiles.ini";	
	std::ifstream in(path);
	if (!in.is_open())
	{
		return std::list <std::string>();
	}
	std::string buffer;
	while (std::getline(in, buffer)) 
	{
		if (buffer.find("Path=") != std::string::npos)
		{
			result.push_back(buffer.substr(5, buffer.size() - 5));
		}			
	}
	return result;
}

std::list<std::string> ParsLib::getAvailableFiles(std::string name) 
{
	if (name.empty())
	{
		return std::list<std::string>();
	}
	std::list <std::string> result;	
	char appData[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, appData);
	std::string path = std::string(appData) + "/Mozilla/Firefox/" + name;
	std::ifstream places(path + "/places.sqlite");
	if (places.is_open())
	{
		result.push_back(path + "/places.sqlite");
	}
	std::ifstream logins(path + "/logins.json");
	if (logins.is_open())
	{
		std::ifstream key3(path + "/key3.db");
		if (key3.is_open())
		{
			result.push_back(path + "/logins.json");
		}			
	}
	char localData[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_LOCAL_APPDATA, NULL, SHGFP_TYPE_CURRENT, localData);
	path = std::string(localData) + "/Mozilla/Firefox/" + name + "/cache2/entries*";
	WIN32_FIND_DATAA FindFileData;
	HANDLE hf = FindFirstFileA(path.c_str(), &FindFileData);
	if (hf != INVALID_HANDLE_VALUE)
	{
		result.push_back(std::string(localData) + "/Mozilla/Firefox/" + name + "/cache2/entries");
	}
	return result;
}


ParsLib::~ParsLib()
{	
	m_lBuffer.clear();
}
