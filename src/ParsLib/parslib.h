#ifndef PARSLIB_H
#define PARSLIB_H

#include <iostream>
#include <windows.h>
#include <Shlwapi.h>
#include <Shlobj.h>     
#include <string>
#include <cstdio>
#include <conio.h>
#include <Wincrypt.h>
#include <fstream>
#include <list>
#include <vector>

#pragma comment (lib, "shlwapi.lib")
#pragma comment (lib, "crypt32.lib")

enum ModelType {
	Passwords,
	Downloads,
	History,
	Cache
};

enum SECItemType {
	siBuffer = 0,
	siClearDataBuffer = 1,
	siCipherDataBuffer,
	siDERCertBuffer,
	siEncodedCertBuffer,
	siDERNameBuffer,
	siEncodedNameBuffer,
	siAsciiNameString,
	siAsciiString,
	siDEROID,
	siUnsignedInteger,
	siUTCTime,
	siGeneralizedTime
};


struct SECItem {
	SECItemType type;
	unsigned char *data;
	size_t len;
};

enum SECStatus {
	SECWouldBlock = -2,
	SECFailure = -1,
	SECSuccess = 0
};


typedef struct PK11SlotInfoStr PK11SlotInfo;
typedef SECStatus(*NSS_Init) (const char *);
typedef SECStatus(*NSS_Shutdown) (void);
typedef PK11SlotInfo * (*PK11_GetInternalKeySlot) (void);
typedef void(*PK11_FreeSlot) (PK11SlotInfo *);
typedef SECStatus(*PK11_Authenticate) (PK11SlotInfo *, int, void *);
typedef SECStatus(*PK11SDR_Decrypt) (SECItem *, SECItem *, void *);

class __declspec(dllexport) ParsLib
{
public:
	ParsLib();
	virtual ~ParsLib();
	virtual std::list<std::string>  showDecryptedPasswords(const std::string &name, int start, int n);
	virtual std::list<std::string>  parseHistory(const std::string &name, const int &nCount, const int &nBookmark);
	virtual std::list<std::string>  parseDownloads(const std::string &name, const int &nCount, const int &nBookmark);
	virtual std::list<std::string>  getFilDataByCacheFilePath(const std::string &name);
	virtual std::string				saveCacheToDisk(const std::string &in,  std::string &size, std::string &out);
	static void                     removeCacheFromDisk(const char* name); 
	virtual	void					initFileList(const std::string &path);
	std::vector<std::string>		getPathList(unsigned int startpos, unsigned int n);
	std::string						getExtensionByContentType(const std::string &name);
	std::string						getDataArray(const std::string &name, const std::string &size);
	virtual int						recordsCountInHistoryTable(const std::string &name);
	virtual int						recordsCountInDownloadsTable(const std::string &name);
	virtual std::list<std::string>  search(const std::string &name, const std::string &szText, const ModelType &eType);	
	int								countPasswords(const std::string &name);
	std::list <std::string>			getUserFolders();
	std::list<std::string>			getAvailableFiles(std::string name);

	std::vector<std::string>		m_vszCachePathList;
	
private:
	PK11_GetInternalKeySlot PK11GetInternalKeySlot;
	PK11_FreeSlot           PK11FreeSlot;
	PK11_Authenticate       PK11Authenticate;
	PK11SDR_Decrypt         PK11SDRDecrypt;
	NSS_Init                fpNSS_INIT;
	NSS_Shutdown            fpNSS_Shutdown;
	char *dupncat(const char *s1, unsigned int n);
	char *dupcat(const char *s1, ...);
	BOOL loadFunctions();
	char *crack(const char *s);
	char*	md5(const char *data, int size);

	std::list<std::string> m_lBuffer;
	//std::string szFileName;
	int m_nRCount;	
};



#endif // PARSLIB_H
